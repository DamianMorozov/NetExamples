# Net.Examples

![Latest Version](https://img.shields.io/github/release/DamianMorozov/Net.Examples.svg)
![Downloads](https://img.shields.io/github/downloads/DamianMorozov/Net.Examples/total.svg)

## WPF.Net.Examples

![](Assets/WPF.Net.Examples.png?raw=true)
![](Assets/WPF.Net.Examples.WebClient.png?raw=true)
![](Assets/WPF.Net.Examples.HttpClient.png?raw=true)
![](Assets/WPF.Net.Examples.Proxy.png?raw=true)
![](Assets/WPF.Net.Examples.Ping.png?raw=true)
![](Assets/WPF.Net.Examples.WebRequest.png?raw=true)
![](Assets/WPF.Net.Examples.AppTheme.png?raw=true)

## Please, if this tool has been useful for you consider to donate
[![Buy me a coffee](Assets/Buy_me_a_coffee.png?raw=true)](https://www.buymeacoffee.com/DamianVM)
